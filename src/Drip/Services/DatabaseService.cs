﻿using Drip.Models.Database;
using LiteDB;

namespace Drip.Services {
    public class DatabaseService {
        private LiteDatabase db;

        private const string urls_collection = "urls";
        private const string files_collection = "files";

        public DatabaseService(LiteDatabase db) {
            this.db = db;
        }

        public void saveShortUrl(ShortUrl shortUrl) {
            var shortUrls = db.GetCollection<ShortUrl>(urls_collection);
            shortUrls.Insert(shortUrl);
            shortUrls.EnsureIndex(x => x.linkId);
        }

        public ShortUrl getShortUrl(string id) {
            var shortUrls = db.GetCollection<ShortUrl>(urls_collection);
            return shortUrls.FindOne(x => x.linkId == id);
        }

        public void saveFile(FileLink fileLink) {
            var files = db.GetCollection<FileLink>(files_collection);
            files.Insert(fileLink);
            files.EnsureIndex(x => x.fileId);
        }
        
        public FileLink getFile(string id) {
            var files = db.GetCollection<FileLink>(files_collection);
            return files.FindOne(x => x.fileId == id);
        }

        public bool deleteFile(string id) {
            var files = db.GetCollection<FileLink>(files_collection);
            return files.Delete(x => x.fileId == id) > 0;
        }
    }
}