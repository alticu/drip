﻿using System.IO;

namespace Drip.Services {
    public class FileStorageService {
        private string basePath;

        public const string uploads_path = "uploads";

        public FileStorageService(string basePath) {
            this.basePath = basePath;
        }

        public bool mkdir(string path) {
            path = fsPath(path);
            if (Directory.Exists(path)) return false;
            Directory.CreateDirectory(path);
            return true;
        }

        public bool upload(Stream source, string path) {
            path = fsPath(path);
            using (var fs = File.Open(path, FileMode.CreateNew)) {
                source.CopyTo(fs);
            }

            return true;
        }

        public Stream openWrite(string path) {
            path = fsPath(path);
            return File.Open(path, FileMode.CreateNew);
        }

        public Stream download(string path) {
            path = fsPath(path);
            return File.OpenRead(path);
        }

        public bool delete(string path) {
            path = fsPath(path);
            if (!File.Exists(path)) return false;
            File.Delete(path);
            return true;
        }

        public bool exists(string path) {
            path = fsPath(path);
            return File.Exists(path);
        }

        private string fsPath(string path) {
            return Path.Combine(basePath, path);
        }
    }
}