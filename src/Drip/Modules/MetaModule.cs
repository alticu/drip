using Driblop.Modules;
using Driblop.Utilities;

namespace Drip.Modules {
    public class MetaModule : DLModule<DripContext> {
        public MetaModule() : base("/m") {
            Get("/", _ =>
                Response.asJsonNet(new {
                    version = DripConfig.version
                })
            );
        }
    }
}