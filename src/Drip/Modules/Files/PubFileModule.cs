﻿using System;
using System.IO;
using System.Security.Cryptography;
using Driblop.Auth.Key;
using Driblop.Modules;
using Drip.Services;
using Drip.Utilities;
using Nancy;
using Nancy.Responses;

namespace Drip.Modules.Files {
    public class PubFileModule : DLModule<DripContext> {
        public PubFileModule() : base("/f") {
            Get("/{fileId}", args => {
                var attach = (string) this.Request.Query.dl == "1";
                var fileLink = serverContext.dbService.getFile((string) args.fileId);
                if (fileLink == null) return Response.AsText("not found").WithStatusCode(HttpStatusCode.NotFound);
                var fs = serverContext.fsService.download(
                    Path.Combine(FileStorageService.uploads_path, fileLink.fileId));
                var contentType = MimeTypes.GetMimeType(fileLink.name);
                if (attach) {
                    var response = new StreamResponse(() => fs, contentType);
                    return response.AsAttachment(fileLink.name);
                } else {
                    // stream the file
                    try {
                        return Response.fromPartialStream(Request, fs, contentType);
                    } catch {
                        return HttpStatusCode.BadRequest;
                    }
                }
            });

            Get("/c/{fileId}", args => {
                // fetch encrypted file
                var stream = (string) this.Request.Query.stream == "1";
                if (stream && !serverContext.config.publicCryptStreaming) {
                    // only admins can stream
                    if (!this.Context.holdsKey()) {
                        return HttpStatusCode.PaymentRequired;
                    }
                }

                var strKey = (string) this.Request.Query.key;
                if (string.IsNullOrEmpty(strKey)) return HttpStatusCode.BadRequest;
                var fileLink = serverContext.dbService.getFile((string) args.fileId);
                if (fileLink == null) return Response.AsText("not found").WithStatusCode(HttpStatusCode.NotFound);
                var responseStream = new MemoryStream();
                using (var aes = Aes.Create()) {
                    aes.IV = fileLink.iv;
                    try {
                        aes.Key = Convert.FromBase64String(strKey);
                    } catch {
                        return HttpStatusCode.BadRequest;
                    }

                    var fs = serverContext.fsService.download(
                        Path.Combine(FileStorageService.uploads_path, fileLink.fileId));
                    var contentType = MimeTypes.GetMimeType(fileLink.name);

                    using (var decryptor = aes.CreateDecryptor(aes.Key, aes.IV))
                    using (var aesStream = new CryptoStream(fs, decryptor, CryptoStreamMode.Read)) {
                        aesStream.CopyTo(responseStream);
                        // TODO: this can be intensive (DOS)
                        if (stream) {
                            // stream the file
                            try {
                                return Response.fromPartialStream(Request, responseStream, contentType);
                            } catch {
                                return HttpStatusCode.BadRequest;
                            }
                        } else {
                            responseStream.Seek(0, SeekOrigin.Begin);
                            return Response.FromStream(responseStream, contentType)
                                .AsAttachment(fileLink.name);
                        }
                    }
                }
            });
        }
    }
}