﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using Driblop.Auth.Key;
using Driblop.Modules;
using Driblop.Utilities;
using Drip.Models.Database;
using Drip.Services;
using Drip.Utilities;
using Nancy;

namespace Drip.Modules.Files {
    public class FileUploadModule : DLModule<DripContext> {
        public FileUploadModule() : base("/f") {
            this.ensureKeyAuth();

            Post("/u", _ => {
                if (!Request.Files.Any()) return HttpStatusCode.BadRequest;

                // Check for length before processing any files
                if (Request.Files.Any(file => file.Value.Length > serverContext.config.maxFileSize))
                    return HttpStatusCode.UnprocessableEntity;

                var fileLinks = new List<Object>();

                foreach (var file in Request.Files) {
                    var fileLink = new FileLink {
                        fileId = StringUtils.secureRandomString(serverContext.config.shortUrlLength,
                            serverContext.config.shortUrlCharset) + Path.GetExtension(file.Name),
                        name = file.Name
                    };

                    serverContext.fsService.upload(file.Value,
                        Path.Combine(FileStorageService.uploads_path, fileLink.fileId));
                    serverContext.dbService.saveFile(fileLink);

                    fileLinks.Add(new {
                        fileId = fileLink.fileId,
                        name = file.Name
                    });
                }

                return Response.asJsonNet(fileLinks);
            });

            Post("/c/u", _ =>
            {
                if (!Request.Files.Any()) return HttpStatusCode.BadRequest;

                // Check for length before processing any files
                if (Request.Files.Any(file => file.Value.Length > serverContext.config.maxFileSize))
                    return HttpStatusCode.UnprocessableEntity;

                var fileLinks = new List<object>();

                foreach (var file in Request.Files) {
                    var iv = CryptoUtils.randomData(128);
                    var key = CryptoUtils.randomData(256);

                    var fileLink = new FileLink {
                        fileId = StringUtils.secureRandomString(serverContext.config.shortUrlLength,
                            serverContext.config.shortUrlCharset) + Path.GetExtension(file.Name),
                        name = file.Name,
                        iv = iv
                    };

                    // Read in file
                    using (var aes = Aes.Create()) {
                        aes.Key = key;
                        aes.IV = iv;

                        using (var encryptor = aes.CreateEncryptor(aes.Key, aes.IV))
                        using (var resultStream =
                            serverContext.fsService.openWrite(
                                Path.Combine(FileStorageService.uploads_path, fileLink.fileId)))
                        {
                            using (var aesStream =
                                new CryptoStream(resultStream, encryptor, CryptoStreamMode.Write))
                            {
                                file.Value.CopyTo(aesStream);
                            }
                        }
                    }

                    serverContext.dbService.saveFile(fileLink);

                    fileLinks.Add(new {
                        fileId = fileLink.fileId,
                        name = fileLink.name,
                        key = Convert.ToBase64String(key)
                    });
                }

                return Response.asJsonNet(fileLinks);
            });

            Delete("/{fileId}", args => {
                var fileId = (string) args.fileId;
                serverContext.fsService.delete(Path.Combine(FileStorageService.uploads_path, fileId));
                var status = serverContext.dbService.deleteFile(fileId);
                return status ? HttpStatusCode.NoContent : HttpStatusCode.NotFound;
            });
        }
    }
}