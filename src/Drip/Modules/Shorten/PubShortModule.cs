﻿using Driblop.Modules;
using Nancy;
using Nancy.Responses;

namespace Drip.Modules.Shorten {
    public class PubShortModule : DLModule<DripContext> {
        public PubShortModule() : base("/s") {
            Get("/{linkId}", args => {
                var shortUrl = serverContext.dbService.getShortUrl((string) args.linkId);
                if (shortUrl == null) return Response.AsText("not found").WithStatusCode(HttpStatusCode.NotFound);
                return Response.AsRedirect(shortUrl.target, RedirectResponse.RedirectType.Temporary);
            });
        }
    }
}