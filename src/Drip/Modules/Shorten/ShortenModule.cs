﻿using Driblop.Auth.Key;
using Driblop.Modules;
using Driblop.Utilities;
using Drip.Models.Database;
using Drip.Models.Requests;
using Nancy.ModelBinding;

namespace Drip.Modules.Shorten {
    public class ShortenModule : DLModule<DripContext> {
        public ShortenModule() : base("/s") {
            this.ensureKeyAuth();

            Post("/c", _ => {
                var req = this.Bind<ShortenRequest>();
                var shortened = new ShortUrl {
                    linkId = StringUtils.secureRandomString(serverContext.config.shortUrlLength,
                        serverContext.config.shortUrlCharset),
                    target = req.url
                };
                serverContext.dbService.saveShortUrl(shortened);
                return Response.asJsonNet(shortened);
            });
        }
    }
}