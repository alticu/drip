﻿using System.Security.Cryptography;

namespace Drip.Utilities {
    public class CryptoUtils {
        public static byte[] randomData(int bits) {
            var result = new byte[bits / 8];
            RandomNumberGenerator.Create().GetBytes(result);
            return result;
        }
    }
}