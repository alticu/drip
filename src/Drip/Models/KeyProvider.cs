﻿using Driblop.Auth.Key;
using Driblop.Configuration;

namespace Drip.Models {
    public class KeyProvider : IKeyProvider {
        public bool verify(string key) {
            var context = SJar.jar.resolve<DripContext>();
            return context.config.keys.Contains(key);
        }
    }
}