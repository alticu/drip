﻿namespace Drip.Models.Database {
    public class ShortUrl : DatabaseObject {
        public string linkId { get; set; }
        public string target { get; set; }
    }
}