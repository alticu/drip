﻿using LiteDB;
using Newtonsoft.Json;

namespace Drip.Models.Database {
    public class DatabaseObject {
        [JsonIgnore]
        [BsonId]
        public ObjectId databaseId { get; set; }
    }
}