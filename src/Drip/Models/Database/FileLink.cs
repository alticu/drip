﻿namespace Drip.Models.Database {
    public class FileLink : DatabaseObject {
        public string name { get; set; }
        public string fileId { get; set; }
        public byte[] iv { get; set; }
    }
}