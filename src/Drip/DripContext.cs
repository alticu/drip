using System;
using System.Collections.Generic;
using Driblop;
using Driblop.Configuration;
using Drip.Services;
using LiteDB;
using Newtonsoft.Json;

namespace Drip {
    public class DripConfig : SConfig {
        [JsonIgnore] public const string version = "0.1.0";
        public string dbFile = "app.lidb";
        public string fsDir = "drip_fs";
        public HashSet<string> keys = new HashSet<string>();

        public int shortUrlLength = 6;
        public string shortUrlCharset = "23456789abdegjkmnpqrvwxyz";
        public int maxFileSize = 16 * 1024 * 1024; // 16M
        public bool publicCryptStreaming = false;
    }

    public class DripContext : SContext<DripConfig>, IDisposable {
        public DripContext(DripConfig config) : base(config) { }
        public LiteDatabase db;
        public DatabaseService dbService;
        public FileStorageService fsService;

        protected override void initialize() {
            base.initialize();

            registerSelf<DripContext>();

            db = new LiteDatabase(config.dbFile);
            dbService = new DatabaseService(db);
            
            fsService = new FileStorageService(config.fsDir);
        }

        public void Dispose() {
            db.Dispose();
        }
    }
}
