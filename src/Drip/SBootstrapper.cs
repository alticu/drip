using Driblop.Auth.Key;
using Drip.Models;
using Nancy;
using Nancy.Bootstrapper;
using Nancy.TinyIoc;

namespace Drip {
    public class SBootstrapper : DefaultNancyBootstrapper {
        protected override void ApplicationStartup(TinyIoCContainer container, IPipelines pipelines) {
            base.ApplicationStartup(container, pipelines);
            
            KeyAuthenticator.install(pipelines, new KeyProvider());
        }
    }
}