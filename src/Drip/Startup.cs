﻿using System.Net;
using Driblop.Configuration;
using Drip.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Nancy.Owin;

namespace Drip {
    public class Startup {
        public const string configFile = "drip.json";

        public void Configure(IApplicationBuilder app, IHostingEnvironment env) {
            if (env.IsDevelopment()) {
                app.UseDeveloperExceptionPage();
            }

            var config = new Configurator<DripConfig>().ensureFile(configFile);
            var context = new DripContext(config);

            context.fsService.mkdir(FileStorageService.uploads_path);

            app.UseOwin(x => x.UseNancy(opts => {
                opts.Bootstrapper = new SBootstrapper();
            }));
        }
    }
}